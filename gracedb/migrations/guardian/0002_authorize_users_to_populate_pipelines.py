# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-11-01 16:19
from __future__ import unicode_literals

from django.db import migrations

# Creates UserObjectPermission objects which allow specific users
# to add events for pipelines.  Based on current production database
# content (27 October 2017)

# List of pipeline names and lists of usernames who should
# be allowed to add events for them
PP_LIST = [
    {
        'pipeline': 'CWB',
        'usernames': [
            'gdb-processor',
            'waveburst',
            'reed.essick@LIGO.ORG',
        ]
    },
    {
        'pipeline': 'CWB2G',
        'usernames': [
            'waveburst',
            'min-a.cho@LIGO.ORG',
            'gracedb.processor',
        ]
    },
    {
        'pipeline': 'Fermi',
        'usernames': [
            'gdb-processor',
            'alexander.urban@LIGO.ORG',
            'grb.exttrig',
            'gracedb.processor',
        ]
    },
    {
        'pipeline': 'gstlal',
        'usernames': [
            'chad.hanna@LIGO.ORG',
            'leo.singer@LIGO.ORG',
            'gracedb.processor',
            'gdb-processor',
            'gstlalcbc',
        ]
    },
    {
        'pipeline': 'gstlal-spiir',
        'usernames': [
            'shinkee.chung@LIGO.ORG',
            'qi.chu@LIGO.ORG',
            'gstlal-spiir',
            'gstlal-spiir-gpu',
        ]
    },
    {
        'pipeline': 'HardwareInjection ',
        'usernames': [
            'peter.shawhan@LIGO.ORG',
            'christopher.biwer@LIGO.ORG',
            'alexander.urban@LIGO.ORG',
            'laleh.sadeghian@LIGO.ORG',
            'patrick.brockill@LIGO.ORG',
            'gstinjector',
            'GRD_INJ',
            'gdb-processor',
            'hinj',
        ]
    },
    {
        'pipeline': 'LIB',
        'usernames': [
            'salvatore.vitale@LIGO.ORG',
            'ryan.lynch@LIGO.ORG',
            'LIB_PE',
            'alexander.pace@LIGO.ORG',
            'oLIB',
        ]
    },
    {
        'pipeline': 'MBTAOnline',
        'usernames': [
            'MbtaAlert',
            'benoit.mours@LIGO.ORG',
        ]
    },
    {
        'pipeline': 'pycbc',
        'usernames': [
            'stephen.fairhurst@LIGO.ORG',
            'duncan.macleod@LIGO.ORG',
            'andrewlawrence.miller@LIGO.ORG',
            'henning.fehrmann@LIGO.ORG',
            'prayush.kumar@LIGO.ORG',
            'pycbclive',
            'ian.harry@LIGO.ORG',
            'karsten.wiesner@LIGO.ORG',
            'collin.capano@LIGO.ORG',
            'joshua.willis@LIGO.ORG',
            'alex.nitz@LIGO.ORG',
            'christopher.biwer@LIGO.ORG',
            'adam.mercer@LIGO.ORG',
            'andrew.lundgren@LIGO.ORG',
            'tito.canton@LIGO.ORG',
            'stanislav.babak@LIGO.ORG',
            'gergely.debreczeni@LIGO.ORG',
            'duncan.brown@LIGO.ORG',
            'badri.krishnan@LIGO.ORG',
            'thomas.dent@LIGO.ORG',
            'saeed.mirshekari@LIGO.ORG',
        ]
    },
    {
        'pipeline': 'SNEWS',
        'usernames': [
            'gdb-processor',
            'alexander.urban@LIGO.ORG',
            'gracedb.processor',
        ]
    },
    {
        'pipeline': 'Swift',
        'usernames': [
            'grb.exttrig',
            'gracedb.processor',
            'gdb-processor',
            'alexander.urban@LIGO.ORG',
        ]
    },
]

def add_permissions(apps, schema_editor):
    User = apps.get_model('auth', 'User')
    Permission = apps.get_model('auth', 'Permission')
    UserObjectPermission = apps.get_model('guardian', 'UserObjectPermission')
    Pipeline = apps.get_model('events', 'Pipeline')
    ContentType = apps.get_model('contenttypes', 'ContentType')

    perm = Permission.objects.get(codename='populate_pipeline')
    ctype = ContentType.objects.get_for_model(Pipeline)
    for pp_dict in PP_LIST:
        pipeline = Pipeline.objects.get(name=pp_dict['pipeline'])

        # Loop over users
        for username in pp_dict['usernames']:

            # Robot users should have been already created by ligoauth 0003,
            # but we have to create human user accounts here
            user, _ = User.objects.get_or_create(username=username)

            # Create UserObjectPermission
            uop, uop_created = UserObjectPermission.objects.get_or_create(
                user=user, permission=perm, content_type=ctype,
                object_pk=pipeline.id)

def remove_permissions(apps, schema_editor):
    pass

class Migration(migrations.Migration):

    dependencies = [
        ('ligoauth', '0003_initial_localuser_and_x509cert_data'),
        ('events', '0003_initial_pipeline_data'),
        ('auth', '0012_create_custom_permissions'),
        ('guardian', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(add_permissions, remove_permissions),
    ]
